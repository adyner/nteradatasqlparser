﻿using System;
using nTeradataSqlParser;

namespace nTeradataSqlParserCmd
{
    class Program
    {
        static void Main(string[] args)
        {
            //var sql = @"select * from dec_db_tmd.coa /* привет */ where 1= 1 and id = 12;
            //    SELECT trim(both '.' from dev_db_dwh.t_coa.coa_num) FROM t_coa_pty_hist";

            /*
            Scanner scanner = new Scanner(sql);
            Lexem currLexem;
            do
            {
                currLexem = scanner.Scan();
                Console.WriteLine(currLexem);
            }
            while (currLexem.Kind != LexemKind.EndOfFile);
            */

            Syntaxer s;
            Identifier obj;

            s = new Syntaxer(@"create multiset volatile table crd2 AS (
                SEL t1.rec1, t2.client_dk, t3.acct_type
                FROM miss_tc as t1
                    join acct as t2
                ON t1.id eq t2.id2
                QUALIFY 1 = row_number() over (partition by t1.rec order by t2.client_dk desc)
            ) WITH DATA NO PRIMARY INDEX ON COMMIT PRESERVE ROWS");
            var dd = s.GetIdentifiersFromDml();
            Console.WriteLine(dd);

            s = new Syntaxer(@"update t_cod from ""030"".t_nsi nor set NOR = nor.data1");
            obj = s.Update();
            Console.WriteLine(obj);

            s = new Syntaxer(@"update temp_f3 as dst set cli = 1 where tgt_id = 1");
            obj = s.Update();
            Console.WriteLine(obj);


            s = new Syntaxer("UPDATE D FROM crm_bi_limit d , (select a from A) t SET liit_d = group_1 where d.a = t.a");
            obj = s.Update();
            Console.WriteLine(obj);

            s = new Syntaxer("update way4_stage_list set cnt = 11 WHERE 1 = id");
            obj = s.Update();
            Console.WriteLine(obj);


            var s1 = new Scanner(@"delete
--select *
from oakb.tbl
--where date = 1");

            var s11 = s1.Scan();
            var s12 = s1.Scan();

            s = new Syntaxer(@"replace procedure adada.adad() begin ins dev42.t_coa select 1, 2, c from dev22_dd.c_coa inner join items on 1 = 1;

create table adada as ddd.dd with data and stat;
    del d;
database adyner;
update r from decv.ddd r where 1 = 1;
end;                
");
            var dmls = s.GetDmlFromText();
            Console.WriteLine(dmls);

            s = new Syntaxer("ins dev42.t_coa select 1, 2, c from dev22_dd.c_coa inner join items on 1 = 1;");
            var objects = s.GetIdentifiersFromDml();
            Console.WriteLine(objects);


            s = new Syntaxer("create set table dddddd.aaaa");
            obj = s.CreateTable();
            Console.WriteLine(obj);


            s = new Syntaxer("CT dddddd.aaaa1");
            obj = s.CreateTable();
            Console.WriteLine(obj);

            s = new Syntaxer("create table dddddd.aaaa2");
            obj = s.CreateTable();
            Console.WriteLine(obj);

            s = new Syntaxer("create multiset volatile table dddddd.aaaa");
            obj = s.CreateTable();
            Console.WriteLine(obj);

            s = new Syntaxer("delete from OAKB_SANBOX.dw_stg_jjjjjagag_templtare all;\n DEL from OAKB_SANBOX.aa all; DELETE FROM AAAA.DDDD");
            var items = s.SplitBySemicolon();

            s = new Syntaxer("delete from OAKB_SANBOX.dw_stg_jjjjjagag_templtare all");
            obj = s.Delete();
            Console.WriteLine(obj);

            s = new Syntaxer("update blah from blah set r = 1;");
            obj = s.Update();
            Console.WriteLine(obj);

            s = new Syntaxer("using upd");
            obj = s.Update();
            Console.WriteLine(obj);

            s = new Syntaxer("upd with concurrent isolated loading dev_db.t_coa1 r1");
            obj = s.Update();
            Console.WriteLine(obj);

            s = new Syntaxer("upd with concurrent isolated loading t_coa ");
            obj = s.Update();
            Console.WriteLine(obj);

            s = new Syntaxer("upd with concurrent isolated loading       r fRom rrr as e , ad.ad a, dd.d as D, dec_22.t1 as r SET blah = 1;");
            obj = s.Update();
            Console.WriteLine(obj);

            s = new Syntaxer("upd with concurrent isolated loading       r FROM ( sel 1 from rrr ) as e , ad.ad a, dd.d as D, dec_22.t1 as r SET blah = 1;");
            obj = s.Update();
            Console.WriteLine(obj);

            s = new Syntaxer("del t from dev_db.t_coa d , (select 1 from dual) a,dev23 t");
            obj = s.Delete();
            Console.WriteLine(obj);

            s = new Syntaxer("del dev_db.t_coa");
            obj = s.Delete();
            Console.WriteLine(obj);

            s = new Syntaxer("del d from dev_db.t_coa f, dejj.sdsh as d ,    dddd.aaa b;");
            obj = s.Delete();
            Console.WriteLine(obj);

            s = new Syntaxer("ins adad.adadad select 1, 2, 1, 3");
            obj = s.Insert();
            Console.WriteLine(obj);

            s = new Syntaxer("ins with no isolated loading adad.adadad select 1, 2, 1, 3");
            obj = s.Insert();
            Console.WriteLine(obj);

            s = new Syntaxer("del f from dev_db.t_coa f");
            obj = s.Delete();
            Console.WriteLine(obj);

            s = new Syntaxer("del f from dev_db.t_coa f, dejj.sdsh d;");
            obj = s.Delete();
            Console.WriteLine(obj);

            s = new Syntaxer("del d from dev_db.t_coa f, dejj.sdsh d;");
            obj = s.Delete();
            Console.WriteLine(obj);
            
            s = new Syntaxer("using merge into d as d1 t_coa f, dejj.sdsh as d ,    dddd.aaa b;");
            obj = s.Merge();
            Console.WriteLine(obj);

            s = new Syntaxer(@"replace procedure dev2_db.sp_ad() begin call sp1(); call d2.sp2(); call d2.sp3(); 
                
                
                call   jkgkglkewooroeroer.dfghjkouytrewertyuioiuytrewertyui();
database adyner;
call my_super_sp();                

                end;");
            var i = s.GetCallSpFromText();

            Console.ReadKey();
        }
    }
}
