﻿using System;
using System.Collections.Generic;
using System.Text;


namespace nTeradataSqlParser
{
    internal class Syntax
    {
        private readonly Scanner _scanner;
        private Lexem _currLexem;
        private string _defaultDatabase;

        private LexemKind CurrentLexemKind
        {
            get
            {
                return _currLexem.Kind;
            }
        } 

        internal Syntax(Scanner scanner)
        {
            _scanner = scanner;
        }

        internal Syntax(string text)
        {
            _scanner = new Scanner(text);
        }



        private void Database()
        {
            Scan();
            if (CurrentLexemKind == LexemKind.Identifier)
            {
                _defaultDatabase = _currLexem.Image;
            }
            else
            {
                throw new ApplicationException("Встретилось " + _currLexem.Image + " когда ожидалась [identifier]");
            }
            Scan();
            if (CurrentLexemKind != LexemKind.Semicolon)
            {
                throw new ApplicationException("Встретилось " + _currLexem.Image + " когда ожидалась [;]");
            }
        }

        private List<Identifier> Select()
        {
            List<Identifier> result = new List<Identifier>();
            do
            {
                Scan();
            }
            while ((CurrentLexemKind != LexemKind.Select) || (CurrentLexemKind != LexemKind.EndOfFile));

            if (CurrentLexemKind == LexemKind.EndOfFile) 
            {
                return result;
            }
            
            do
            {
                Scan();
                if ((CurrentLexemKind == LexemKind.Trim) || (CurrentLexemKind == LexemKind.Extract) || (CurrentLexemKind == LexemKind.Substring))
                {
                    var trimResult = Trim();
                    result.AddRange(trimResult);
                }
            }
            while ((CurrentLexemKind != LexemKind.From) || (CurrentLexemKind != LexemKind.EndOfFile));

            if (CurrentLexemKind == LexemKind.EndOfFile)
            {
                return result;
            }

            var fromResult = From();
            result.AddRange(fromResult);

            return result;
        }

        private Identifier ReadIdentifier()
        {
            string targetDatabaseName = _currLexem.Image;
            string targetObjName;
            Scan();
            if (CurrentLexemKind == LexemKind.Dot)
            {
                Scan();
                targetObjName = _currLexem.Image;
            }
            else
            {
                targetObjName = targetDatabaseName;
                targetDatabaseName = _defaultDatabase;
            }

            return new Identifier { DatabaseName = targetDatabaseName, ObjectName = targetObjName };
        }

        private List<Identifier> From()
        {
            List<Identifier> result = new List<Identifier>();

            Scan();
            if (CurrentLexemKind == LexemKind.Identifier)
            {
                var id = ReadIdentifier();
                result.Add(id);
            }
            else if (CurrentLexemKind == LexemKind.LeftBracket)
            {
                Scan();
                if (CurrentLexemKind == LexemKind.Select)
                {
                    int brasketBalance = 1;
                    StringBuilder sb = new StringBuilder();
                    do
                    {
                        if (brasketBalance != 0)
                        {
                            sb.AppendFormat("{0} ", _currLexem.Image);
                        }

                        Scan();
                        if (CurrentLexemKind == LexemKind.RightBracket)
                        {
                            brasketBalance -= 1;
                        }

                        if (CurrentLexemKind == LexemKind.LeftBracket)
                        {
                            brasketBalance += 1;
                        }
                    }
                    while (brasketBalance != 0);

                    var s = new Syntax(sb.ToString());
                    result.AddRange(s.Select());
                }
            }
            else
            {
                throw new ApplicationException("Встретилось " + _currLexem.Image + " когда ожидалась [identifier]");
            }
            return result;
        }

        private List<Identifier> Trim()
        {
            Scan(); // (
            if (CurrentLexemKind != LexemKind.LeftBracket)
            {
                throw new ApplicationException("Встретилось " + _currLexem.Image + " когда ожидалась (");
            }
            int brasketBalance = 1;
            StringBuilder sb = new StringBuilder();
            do
            {
                Scan();
                if (CurrentLexemKind == LexemKind.RightBracket)
                {
                    brasketBalance -= 1;
                }

                if (CurrentLexemKind == LexemKind.LeftBracket)
                {
                    brasketBalance += 1;
                }

                if (brasketBalance != 0)
                {
                    sb.AppendFormat("{0} ", _currLexem.Image);
                }
            }
            while (brasketBalance != 0);

            var s = new Syntax(sb.ToString());
            return s.Select();
        }

        private StatementParseResult Insert()
        {
            var result = new StatementParseResult();
            result.ActionKind = _currLexem;

            Scan();
            if (CurrentLexemKind == LexemKind.Into)
            {
                Scan();
            }

            result.TargetObjectId = ReadIdentifier();
            
            do
            {
                Scan();
            }
            while ((CurrentLexemKind != LexemKind.Select) || (CurrentLexemKind != LexemKind.EndOfFile));

            if (CurrentLexemKind == LexemKind.Select)
            {
                result.SourceObjects = Select();
            }
            return result;
        }

        private void SkipIsolatedLoading()
        {
            Scan();
            if (CurrentLexemKind == LexemKind.No)
            {
                Scan();
            }

            if (CurrentLexemKind == LexemKind.Concurrent)
            {
                Scan();
            }
            Scan(); //ISOLATED
            Scan(); //LOADING
        }

        private StatementParseResult Update()
        {
            var result = new StatementParseResult();
            result.ActionKind = _currLexem;

            Scan();
            if (CurrentLexemKind == LexemKind.With)
            {
                SkipIsolatedLoading();
            }

            var updatableTableId =  ReadIdentifier();
            Scan();
            

            return result;
        }

        public void Process()
        {

            do
            {
                Scan();
            }
            while (CurrentLexemKind != LexemKind.Insert);

            Insert();

        }
        

        private void Scan()
        {
            _currLexem = _scanner.Scan();
            Console.WriteLine(_currLexem);
        }

        private void NDefineProcedure()
        {
            string spName;
            do
            {
                Scan();
                if ((_currLexem.Kind == LexemKind.Create) || (_currLexem.Kind == LexemKind.Replace))
                {
                    Scan(); // try get procedure
                    if (_currLexem.Kind == LexemKind.Procedure)
                    {
                        Scan();
                        if (_currLexem.Kind == LexemKind.Identifier)
                        {
                            spName = _currLexem.Image;
                            Scan();
                            if (_currLexem.Kind == LexemKind.Dot)
                            {
                                Scan();
                                spName = spName + "." + _currLexem.Image;
                            }
                        }
                        else
                        {
                            throw new ApplicationException("Встретилось " + _currLexem.Image + " когда ожидалась [identifier]");
                        }

                        do //скучное место - просто набор параметров
                        {
                            Scan();
                        }
                        while (CurrentLexemKind != LexemKind.Begin);
                    }
                    else
                    {
                        throw new ApplicationException("Встретилось " + _currLexem.Image + " когда ожидалась PROCEDURE");
                    }
                }
            }
            while (CurrentLexemKind != LexemKind.EndOfFile);
        }

    }
}
