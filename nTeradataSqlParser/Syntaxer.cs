﻿using System.Collections.Generic;
using System.Text;


namespace nTeradataSqlParser
{
    public class Syntaxer
    {
        private readonly Scanner _scanner;
        private Lexem _currLexem;
        private string _defaultDatabase = null;

        public Syntaxer(Scanner scanner)
        {
            _scanner = scanner;
        }

        public Syntaxer(string text) : this(new Scanner(text))
        {
        }

        private void SkipIsolatedLoading()
        {
            Scan();
            if (CurrentLexemKind == LexemKind.No)
            {
                Scan();
            }

            if (CurrentLexemKind == LexemKind.Concurrent)
            {
                Scan();
            }

            Scan(); //ISOLATED
            Scan(); //LOADING
        }

        public List<string> SplitBySemicolon()
        {
            List<string> result = new List<string>();
            do
            {
                StringBuilder sb = new StringBuilder();
                Scan();
                while ((CurrentLexemKind != LexemKind.Semicolon) && (CurrentLexemKind != LexemKind.EndOfFile))
                {
                    sb.AppendFormat("{0} ", CurrentLexemImage);
                    Scan();
                }
                result.Add(sb.ToString());
            }
            while ((CurrentLexemKind != LexemKind.EndOfFile))
            ;
            return result;
        }

        public Identifier CreateTable()
        {
            if (_currLexem == null)
            {
                Scan();
            }

            while (! ((CurrentLexemKind == LexemKind.Create) || (CurrentLexemKind == LexemKind.CT)))
            {
                Scan();
                if (CurrentLexemKind == LexemKind.EndOfFile)
                {
                    return null;
                }
            }

            if (CurrentLexemKind == LexemKind.CT)
            {
                Scan();
                return ReadIdentifier();
            }

            Scan();
            if ((CurrentLexemKind == LexemKind.Set) || (CurrentLexemKind == LexemKind.Table) || (CurrentLexemKind == LexemKind.Multiset) || (CurrentLexemKind == LexemKind.Global) || (CurrentLexemKind == LexemKind.Volatile))
            {
                while (CurrentLexemKind != LexemKind.Table) 
                {
                    Scan();
                    if (CurrentLexemKind == LexemKind.EndOfFile)
                    {
                        return null;
                    }
                }
                Scan();
                if (CurrentLexemKind == LexemKind.Identifier)
                {
                    return ReadIdentifier();
                }
            }

            return null;
        }

        public Identifier Update()
        {
            while ((_currLexem == null) || (CurrentLexemKind != LexemKind.Update))
            {
                Scan();
                if (CurrentLexemKind == LexemKind.EndOfFile)
                {
                    return null;
                }
            }

            Scan(); // next lexem after [update]
            if (CurrentLexemKind == LexemKind.With)
            {
                SkipIsolatedLoading();
            }
            var firstId = ReadIdentifier();
            if ((firstId.DatabaseName != null) || (CurrentLexemKind == LexemKind.Set) || (CurrentLexemKind == LexemKind.As))
            {
                return firstId;
            }

            if (CurrentLexemKind == LexemKind.From) 
            {
                Dictionary<string, Identifier> objects = new Dictionary<string, Identifier>();
                do
                {
                    Scan();
                    bool isSubQuery = true;
                    Identifier tableName = null;
                    if (LexemKind.LeftBracket == CurrentLexemKind)
                    {
                        int bracketBalance = 1;
                        do
                        {
                            Scan();
                            if (CurrentLexemKind == LexemKind.RightBracket)
                            {
                                bracketBalance -= 1;
                            }

                            if (CurrentLexemKind == LexemKind.LeftBracket)
                            {
                                bracketBalance += 1;
                            }
                        }
                        while ((bracketBalance != 0) && (CurrentLexemKind != LexemKind.EndOfFile));
                    }
                    else
                    {
                        tableName = ReadIdentifier();
                        isSubQuery = false;
                    }
                    
                    if (LexemKind.As == CurrentLexemKind)
                    {
                        Scan();
                    }
                    if (CurrentLexemKind == LexemKind.Identifier)
                    {
                        if (! isSubQuery)
                        {
                            objects[CurrentLexemImage.ToUpper()] = tableName;
                        }
                        Scan();
                    }
                }
                while ((CurrentLexemKind != LexemKind.EndOfFile) && (CurrentLexemKind != LexemKind.Set))
                ;

                Identifier objId;
                if (objects.TryGetValue(firstId.ObjectName.ToUpper(), out objId))
                {
                    return objId;
                }
                else
                {
                    if (CurrentLexemKind == LexemKind.Set)
                    {
                        return firstId;
                    }
                }
            }
            return null;
        }

        public Identifier Delete()
        {
            while ((_currLexem == null) || (CurrentLexemKind != LexemKind.Delete))
            {
                Scan();
                if (CurrentLexemKind == LexemKind.EndOfFile)
                {
                    return null;
                }
            }

            Scan(); // next lexem after [delete]
            if (CurrentLexemKind == LexemKind.With)
            {
                SkipIsolatedLoading();
            }

            Identifier firstId = null;
            if (CurrentLexemKind == LexemKind.Identifier)
            {
                firstId = ReadIdentifier();
                if (firstId.DatabaseName != null)
                {
                    return firstId;
                }
            }

            if (CurrentLexemKind == LexemKind.From)
            {
                Dictionary<string, Identifier> objects = new Dictionary<string, Identifier>();
                do
                {
                    Scan();
                    bool isSubQuery = true;
                    Identifier tableName = null;
                    if (LexemKind.LeftBracket == CurrentLexemKind)
                    {
                        int bracketBalance = 1;
                        do
                        {
                            Scan();
                            if (CurrentLexemKind == LexemKind.RightBracket)
                            {
                                bracketBalance -= 1;
                            }

                            if (CurrentLexemKind == LexemKind.LeftBracket)
                            {
                                bracketBalance += 1;
                            }
                        }
                        while ((bracketBalance != 0) && (CurrentLexemKind != LexemKind.EndOfFile));
                    }
                    else
                    {
                        tableName = ReadIdentifier();
                        if (firstId == null)
                        {
                            return tableName;
                        }
                        isSubQuery = false;
                    }

                    if (LexemKind.As == CurrentLexemKind)
                    {
                        Scan();
                    }
                    if (CurrentLexemKind == LexemKind.Identifier)
                    {
                        if (!isSubQuery)
                        {
                            objects[CurrentLexemImage] = tableName;
                        }
                        Scan();
                    }
                }
                while ((CurrentLexemKind != LexemKind.Semicolon) && (CurrentLexemKind != LexemKind.EndOfFile) && (CurrentLexemKind != LexemKind.All) && (CurrentLexemKind != LexemKind.Where))
                ;

                Identifier objId;
                if (objects.TryGetValue(firstId.ObjectName, out objId))
                {
                    return objId;
                }
            }
            return firstId; 
        }

        public Identifier Insert()
        {
            while ((_currLexem == null) || (CurrentLexemKind != LexemKind.Insert))
            {
                Scan();
                if (CurrentLexemKind == LexemKind.EndOfFile)
                {
                    return null;
                }
            }

            Scan();
            if (CurrentLexemKind == LexemKind.With)
            {
                SkipIsolatedLoading();
            }

            if (CurrentLexemKind == LexemKind.Into)
            {
                Scan();
            }

            return ReadIdentifier();
        }

        public Identifier Merge()
        {
            while ((_currLexem == null) || (CurrentLexemKind != LexemKind.Merge))
            {
                Scan();
                if (CurrentLexemKind == LexemKind.EndOfFile)
                {
                    return null;
                }
            }

            Scan();
            if (CurrentLexemKind == LexemKind.With)
            {
                SkipIsolatedLoading();
            }

            if (CurrentLexemKind == LexemKind.Into)
            {
                Scan();
            }

            return ReadIdentifier();
        }

        public List<SpStatament> GetDmlFromText()
        {
            List<SpStatament> result = new List<SpStatament>();
            if (_currLexem == null)
            {
                Scan();
            }

            string currentDatabase = null;
            do
            {
                while ((CurrentLexemKind != LexemKind.Merge) && (CurrentLexemKind != LexemKind.Update) && (CurrentLexemKind != LexemKind.Insert) && (CurrentLexemKind != LexemKind.Delete) && (CurrentLexemKind != LexemKind.CT) && (CurrentLexemKind != LexemKind.Create) && (CurrentLexemKind != LexemKind.Database))
                {
                    Scan();
                    if (CurrentLexemKind == LexemKind.EndOfFile)
                    {
                        return result;
                    }
                }
                
                if (CurrentLexemKind == LexemKind.Database)
                {
                    Scan();
                    currentDatabase = CurrentLexemImage;
                }
                else
                {
                    StringBuilder sb = new StringBuilder();
                    while ((CurrentLexemKind != LexemKind.EndOfFile) && (CurrentLexemKind != LexemKind.Semicolon))
                    {
                        sb.AppendFormat("{0} ", CurrentLexemImage);
                        Scan();
                    }
                    result.Add(new SpStatament { DefaultDatabase = currentDatabase, Sql = sb.ToString() });
                }
            }
            while (CurrentLexemKind != LexemKind.EndOfFile);
            return result;
        }

        public List<Identifier> GetCallSpFromText()
        {
            List<Identifier> result = new List<Identifier>();
            if (_currLexem == null)
            {
                Scan();
            }

            string currentDatabase = null;
            do
            {
                while ((CurrentLexemKind != LexemKind.Call) && (CurrentLexemKind != LexemKind.Database))
                {
                    Scan();
                    if (CurrentLexemKind == LexemKind.EndOfFile)
                    {
                        return result;
                    }
                }

                if (CurrentLexemKind == LexemKind.Database)
                {
                    Scan();
                    currentDatabase = CurrentLexemImage;
                }
                else 
                {
                    Scan();
                    var localId = ReadIdentifier();
                    if (string.IsNullOrEmpty(localId.DatabaseName))
                    {
                        localId.DatabaseName = currentDatabase;
                    }
                    result.Add(localId);
                }
            }
            while (CurrentLexemKind != LexemKind.EndOfFile);
            return result;
        }

        public List<Identifier> GetIdentifiersFromDml()
        {
            List<Identifier> result = new List<Identifier>();

            if (_currLexem == null)
            {
                Scan();
            }

            do
            {
                while ((CurrentLexemKind != LexemKind.Identifier) && (CurrentLexemKind != LexemKind.EndOfFile))
                {
                    Scan();
                }
                
                if (CurrentLexemKind == LexemKind.Identifier)
                {
                    var id = ReadIdentifier();
                    result.Add(id);
                    if (CurrentLexemKind == LexemKind.Dot)
                    {
                        Scan();
                    }
                }
            }
            while (CurrentLexemKind != LexemKind.EndOfFile);
            return result;
        }

        private Identifier ReadIdentifier()
        {
            string targetDatabaseName = _currLexem.Image;
            string targetObjName;
            Scan();
            if (CurrentLexemKind == LexemKind.Dot)
            {
                Scan();
                targetObjName = _currLexem.Image;
                Scan();
            }
            else
            {
                targetObjName = targetDatabaseName;
                targetDatabaseName = _defaultDatabase;
            }

            return new Identifier { DatabaseName = targetDatabaseName, ObjectName = targetObjName };
        }

        private void Scan()
        {
            _currLexem = _scanner.Scan();
        }

        private LexemKind CurrentLexemKind
        {
            get
            {
                return _currLexem.Kind;
            }
        }

        private string CurrentLexemImage
        {
            get
            {
                return _currLexem.Image;
            }
        }
    }
}
