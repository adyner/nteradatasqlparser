﻿using System;
using System.ComponentModel;
using System.Collections.Generic;

namespace nTeradataSqlParser
{
    public class Scanner
    {
        private readonly string _codeText;
        private int _position = 0;
        private int _lineNumber = 1;
        private int _inLinePosition = 0;

        private Dictionary<char, byte> _newLineCharacters = new Dictionary<char, byte> {
            { '\n', 1},
            { '\r', 1}
        };

        private Dictionary<char, byte> _spaceCharacters = new Dictionary<char, byte> {
            { ' ',  1},
            { '\n', 1},
            { '\r', 1},
            { '\t', 1}
        };

        private char CurrChar
        {
            get
            {
                return _codeText[_position];
            }
        }

        /// <summary>
        /// Возвращает исходный код
        /// </summary>
        public string CodeText
        {
            get { return _codeText; }
        }

        private char ReadSymbol()
        {
            _position++;
            if (CurrChar == '\n')
            {
                _lineNumber++;
                _inLinePosition = 0;
            }
            else
            {
                _inLinePosition++;
            }
            return CurrChar;
        }
        
        public Scanner(string text)
        {
            _codeText = text + Convert.ToString('\0');

            var lexemKindType = typeof(LexemKind);
            foreach (LexemKind lexemKind in Enum.GetValues(lexemKindType))
            {
                var memberInfo = lexemKindType.GetMember(lexemKind.ToString());
                var attributes = memberInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attributes.Length > 0)
                {
                    var desc = (DescriptionAttribute)attributes[0];
                    _lexemImages[desc.Description] = lexemKind;
                }                
            }
        }

        public Lexem Scan()
        {
            try
            {
                while (_spaceCharacters.ContainsKey(CurrChar))
                {
                    ReadSymbol();
                }

                // Комментарий типа --
                if ((CurrChar == '-') && (_codeText[_position + 1] == '-'))
                {
                    _position += 2;
                    while (! _newLineCharacters.ContainsKey(CurrChar))
                    {
                        ReadSymbol();
                    }
                    return Scan();
                }

                //Комментарий вида /* ... */
                if ((CurrChar == '/') && (_codeText[_position + 1] == '*'))
                {
                    _position += 2;
                    do
                    {
                        ReadSymbol();
                    } while ((CurrChar != '/') || (_codeText[_position - 1] != '*'));

                    ReadSymbol();
                    return Scan();
                }

                if (CurrChar == '\0')
                {
                    return new Lexem {
                        Kind = LexemKind.EndOfFile,
                        Image = "Конец исходного кода",
                        LineNumber = _lineNumber,
                        InLinePosition = _inLinePosition
                    };
                }

                if ((CurrChar >= '0') && (CurrChar <= '9'))
                {
                    int i = 1;
                    int startLexemPosition = _position;
                    ReadSymbol();
                    while ((CurrChar >= '0') && (CurrChar <= '9'))
                    {
                        i++;
                        ReadSymbol();
                    }

                    if (CurrChar == '.')
                    {
                        i += 2; //Учли точку и символ
                        ReadSymbol();
                        while ((CurrChar >= '0') && (CurrChar <= '9'))
                        {
                            i++;
                            ReadSymbol();
                        }

                        return new Lexem { Kind = LexemKind.ConstFloat, Image = _codeText.Substring(startLexemPosition, i), LineNumber = _lineNumber, InLinePosition = _inLinePosition };
                    }
                    return new Lexem { Kind = LexemKind.ConstInteger, Image = _codeText.Substring(startLexemPosition, i), LineNumber = _lineNumber, InLinePosition = _inLinePosition };
                }

                if (CurrChar == '"')
                {
                    int startLexemPosition = _position;
                    ReadSymbol();
                    int i = 1;
                    while ((CurrChar != '"') || ((CurrChar == '"') && _codeText[_position + 1] == '"'))
                    {
                        if ((CurrChar == '"') && _codeText[_position + 1] == '"')
                        {
                            _position += 2;
                            i += 2;
                        }
                        else
                        {
                            ReadSymbol();
                            i++;
                        }
                    }

                    _position++;
                    i++;
                    return new Lexem { Kind = LexemKind.Identifier, Image = _codeText.Substring(startLexemPosition, i), LineNumber = _lineNumber, InLinePosition = _inLinePosition };
                }

                //строковый литерал
                if (CurrChar == '\'')
                {
                    int startLexemPosition = _position;
                    _position++;
                    int i = 1;
                    while (CurrChar != '\'' || ((CurrChar == '\'') && _codeText[_position + 1] == '\''))
                    {
                        if ((CurrChar == '\'') && _codeText[_position + 1] == '\'')
                        {
                            _position += 2;
                            i += 2;
                        }
                        else
                        {
                            _position++;
                            i++;
                        }
                    }
                    ReadSymbol();
                    i++;
                    return new Lexem { Kind = LexemKind.ConstString, Image = _codeText.Substring(startLexemPosition, i), LineNumber = _lineNumber, InLinePosition = _inLinePosition };
                }

                if (CurrChar == ':') {
                    int startLexemPosition = _position;
                    _position++;
                    int i = 1;

                    while (
                            (CurrChar >= '0' && CurrChar <= '9') ||
                            (CurrChar >= 'a' && CurrChar <= 'z') ||
                            (CurrChar >= 'A' && CurrChar <= 'Z') ||
                            (CurrChar == '_') ||
                            (CurrChar == '$') ||
                            (CurrChar == '#')
                           )
                    {
                        _position++;
                        i++;
                    }

                    var image = _codeText.Substring(startLexemPosition, i);
                    return new Lexem { Kind = LexemKind.Variable, Image = image, LineNumber = _lineNumber, InLinePosition = _inLinePosition };
                }


                if ((CurrChar >= 'a' && CurrChar <= 'z') ||
                    (CurrChar >= 'A' && CurrChar <= 'Z') ||
                    (CurrChar == '_') ||
                    (CurrChar == '$') ||
                    (CurrChar == '#') 
                   )
                {
                    int startLexemPosition = _position;
                    _position++;
                    int i = 1;

                    while (
                            (CurrChar >= '0' && CurrChar <= '9') ||
                            (CurrChar >= 'a' && CurrChar <= 'z') ||
                            (CurrChar >= 'A' && CurrChar <= 'Z') ||
                            (CurrChar == '_') ||
                            (CurrChar == '$') ||
                            (CurrChar == '#')
                           )
                    {
                        _position++;
                        i++;
                    }

                    var image = _codeText.Substring(startLexemPosition, i);
                    return TryResolveLexem(image);
                }

                if (CurrChar == '=')
                {
                    _position++;
                    return new Lexem { Image = "=", Kind = LexemKind.EQ, LineNumber = _lineNumber, InLinePosition = _inLinePosition };
                }

                if (CurrChar == '>')
                {
                    _position++;
                    if (CurrChar == '=')
                    {
                        _position++;
                        return new Lexem { Kind = LexemKind.GE, Image = ">=", LineNumber = _lineNumber, InLinePosition = _inLinePosition };
                    }
                    return new Lexem { Kind = LexemKind.GT, Image = ">", LineNumber = _lineNumber, InLinePosition = _inLinePosition };
                }

                if (CurrChar == '<')
                {
                    _position++;
                    if (CurrChar == '=')
                    {
                        _position++;
                        return new Lexem { Kind = LexemKind.LE, Image = "<=", LineNumber = _lineNumber, InLinePosition = _inLinePosition };
                    }
                    if (CurrChar == '>')
                    {
                        _position++;
                        return new Lexem { Kind = LexemKind.NE, Image = "<>", LineNumber = _lineNumber, InLinePosition = _inLinePosition };
                    }
                    return new Lexem { Kind = LexemKind.LT, Image = "<", LineNumber = _lineNumber, InLinePosition = _inLinePosition };
                }

                LexemKind resultLexemKind;
                if (_singleCharLexemImages.TryGetValue(CurrChar, out resultLexemKind))
                {
                    return new Lexem { Kind = resultLexemKind, Image = _codeText[_position++].ToString(), LineNumber = _lineNumber, InLinePosition = _inLinePosition };
                }

                _position++;
                return new Lexem { Kind = LexemKind.Error, Image = "Parse fail", LineNumber = _lineNumber, InLinePosition = _inLinePosition };
            }
            catch (IndexOutOfRangeException)
            {
                return new Lexem { Kind = LexemKind.EndOfFile, Image = "End of file", LineNumber = _lineNumber, InLinePosition = _inLinePosition };
            }
        }

        private Lexem TryResolveLexem(string lexemText)
        {
            var searchText = lexemText.ToUpper();
            LexemKind resultLexemKind;
            if (_lexemImages.TryGetValue(searchText, out resultLexemKind))
            {
                return new Lexem { Kind = resultLexemKind, Image = lexemText, LineNumber = _lineNumber, InLinePosition = _inLinePosition };
            }
            else
            {
                return new Lexem { Kind = LexemKind.Identifier, Image = lexemText, LineNumber = _lineNumber, InLinePosition = _inLinePosition };
            }
        }

        private Dictionary<char, LexemKind> _singleCharLexemImages = new Dictionary<char, LexemKind>
        {
            { ',', LexemKind.Comma },
            { '=', LexemKind.EQ},
            { '>', LexemKind.LeftGuillemet },
            { '<', LexemKind.RightGuillemet },
            { '.', LexemKind.Dot },
            { ';', LexemKind.Semicolon },
            { '+', LexemKind.Plus},
            { '-', LexemKind.Minus},
            { '*', LexemKind.Asterisk},
            { '/', LexemKind.Slash},
            { '(', LexemKind.LeftBracket},
            { ')', LexemKind.RightBracket},
            { '?', LexemKind.BindVariable},
            { '@', LexemKind.At}
        };

        private Dictionary<string, LexemKind> _lexemImages = new Dictionary<string, LexemKind> {
            { "AVERAGE", LexemKind.Avg},

            { "CHARACTER_LENGTH", LexemKind.Chars},
            { "CHAR_LENGTH", LexemKind.Chars},
            { "COMMIT", LexemKind.Et },

            { "DEL", LexemKind.Delete},
            { "DEC", LexemKind.Decimal},

            { "EQ", LexemKind.EQ},
            { "GE", LexemKind.GE},
            { "GT", LexemKind.GT},

            { "INT", LexemKind.Integer },
            { "INS", LexemKind.Insert },
            { "LOCKING", LexemKind.Lock},

            { "MAXIMUM", LexemKind.Max },
            { "MINIMUM", LexemKind.Min },

            { "!=", LexemKind.NE},
            
            { "STAT", LexemKind.Statistics},
            { "STATS", LexemKind.Statistics },
            { "SEL", LexemKind.Select},
    
            { "UPD", LexemKind.Update },
            
        }
        ;
    }
}
