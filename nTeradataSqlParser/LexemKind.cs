﻿using System.ComponentModel;

namespace nTeradataSqlParser
{
    public enum LexemKind
    {
        Unknown = 0,
        Identifier = 10,
        ConstFloat = 70,
        ConstInteger = 75,
        ConstString = 80,

        [Description("?")]
        BindVariable = 82,

        Variable = 84,

        Error = 770,
        EndOfFile = 880,

        //A-1000
        [Description("ABORT")]
        Abort = 1040,

        [Description("ABS")]
        Abs = 1050,

        [Description("ADD_MONTHS")]
        AddMonths = 1060,

        [Description("ALTER")]
        Alter = 1100,

        [Description("ALL")]
        All = 1200,

        [Description("AND")]
        And = 1300,

        [Description("ANY")]
        Any = 1350,

        [Description("AS")]
        As = 1400,

        [Description("*")]
        Asterisk = 1450,

        [Description("@")]
        At = 1500,

        [Description("Attr")]
        Attr = 1600,

        [Description("ACCESS")]
        Access = 1700,

        [Description("ACTIVITY_COUNT")]
        ActivityCount = 1800,

        [Description("AVG")]
        Avg = 1850,

        //B-2000
        [Description("BEGIN")]
        Begin = 2100,

        [Description("BETWEEN")]
        Between = 2150,

        [Description("BIGINT")]
        Bigint = 2200,

        [Description("BINARY")]
        Binary = 2250,

        [Description("BLOB")]
        Blob = 2270,

        [Description("BOTH")]
        Both = 2290,

        [Description("BT")]
        Bt = 2300,

        [Description("BY")]
        By = 2400,

        [Description("BYTE")]
        Byte = 2450,

        [Description("BYTEINT")]
        ByteInt = 2460,


        //C-3000
        [Description("CALL")]
        Call = 3100,

        [Description("CASE")]
        Case = 3150,

        [Description("CASE_N")]
        CaseN = 3170,

        [Description("CASESPECIFIC")]
        Casespecific = 3200,

        [Description("CAST")]
        Cast = 3300,

        [Description("CHAR")]
        Char = 3320,

        [Description("CHARACTER")]
        Character = 3330,

        [Description("CHARS")]
        Chars = 3340,

        [Description("CLOB")]
        Clob = 3360,

        [Description("CLOSE")]
        Close = 3380,

        [Description("CROSS")]
        Cross = 3383,

        [Description("COALESCE")]
        Coalesce = 3385,

        [Description("COS")]
        Cos = 3390,

        [Description("COUNT")]
        Count = 3400,

        [Description(",")]
        Comma = 3450,        

        [Description("COMPRESS")]
        Compress = 3475,

        [Description("CONCURRENT")]
        Concurrent = 3500,

        [Description("CONSUME")]
        Consume = 3600,

        [Description("CONTINUE")]
        Continue = 3800,

        [Description("CONDITION")]
        Condition = 3850,

        [Description("COLLECT")]
        Collect = 3900,

        [Description("CREATE")]
        Create = 3920,

        [Description("CT")]
        CT = 3930,

        [Description("CS")]
        CS = 3935,

        [Description("CURSOR")]
        Cursor = 3940,

        [Description("CURRENT")]
        Current = 3950,

        [Description("CURRENT_DATE")]
        CurrentDate = 3952,

        [Description("CURRENT_ROLE")]
        CurrentRole = 3954,

        [Description("CURRENT_TIME")]
        CurrentTime = 3956,

        [Description("CURRENT_TIMESTAMP")]
        CurrentTimestamp = 3958,

        [Description("CURRENT_USER")]
        CurrentUser = 3970,


        //D-4000
        [Description("DATABASE")]
        Database = 4100,

        [Description("DATE")]
        Date = 4150,

        [Description("DAY")]
        Day = 4170,

        [Description("DECIMAL")]
        Decimal = 4180,

        [Description("DECLARE")]
        Declare = 4200,

        [Description("DECOMPRESS")]
        Decompress = 4250,

        [Description("DEFAULT")]
        Default = 4300,

        [Description("DELETE")]
        Delete = 4350,

        [Description("DESC")]
        Desc = 4360,

        [Description("DISTINCT")]
        Distinct = 4365,

        [Description(".")]
        Dot = 4370,

        [Description("DOUBLE")]
        Double = 4380,


        [Description("DYNAMIC")]
        Dynamic = 4400,


        //E-5000
        [Description("ELSE")]
        Else = 5050,

        [Description("END")]
        End = 5100,

        [Description("ET")]
        Et = 5200,

        [Description("EXCEPT")]
        Except = 5250,

        [Description("EXEC")]
        Exec = 5300,

        [Description("EXECUTE")]
        Execute = 5400,

        [Description("EXISTS")]
        Exists = 5600,

        [Description("EXIT")]
        Exit = 5600,

        [Description("EXTRACT")]
        Extract = 5800,

        [Description("=")]
        EQ = 5900,
        
            
        //F-6000
        [Description("FIRST")]
        First = 6100,

        [Description("FETCH")]
        Fetch = 6200,
        
        [Description("FLOAT")]
        Float = 6400,

        [Description("FROM")]
        From = 6600,

        [Description("FOR")]
        For = 6700,

        [Description("FORMAT")]
        Format = 6800,

        [Description("FUNCTION")]
        Function = 6900,


        //G-7000   
        [Description("GLOBAL")]
        Global = 7100,

        [Description("GROUP")]
        Group = 7200,

        [Description("GROUPING")]
        Grouping = 7300,

        [Description(">=")]
        GE = 7400,

        [Description(">")]
        GT = 7500,


        //H-8000
        [Description("HANDLER")]
        Handler = 8100,

        [Description("HASH")]
        Hash = 8200,

        [Description("HASHAMP")]
        Hashamp = 8300,

        [Description("HASHBUCKET")]
        Hashbucket = 8400,

        [Description("HASHROW")]
        Hashrow = 8500,

        [Description("HAVING")]
        Having = 8600,

        [Description("HOUR")]
        Hour = 8700,


        //I-9000
        [Description("IN")]
        In = 9040,

        [Description("INDEX")]
        Index = 9050,

        [Description("INTO")]
        Into = 9100,

        [Description("INNER")]
        Inner = 9200,

        [Description("INTEGER")]
        Integer = 9220,

        [Description("INTERSECT")]
        Intersect = 9250,

        [Description("INSERT")]
        Insert = 9270,

        [Description("INTERVAL")]
        Interval = 9280,

        [Description("IS")]
        Is = 9290,

        [Description("ISOLATED")]
        Isolated = 9300,


        //J=10000
        [Description("JOIN")]
        Join = 10300,

        //K-11000
        [Description("KEY")]
        Key = 11500,


        //L-12000
        [Description("<=")]
        LE = 12050,

        [Description("LEADING")]
        Leading = 12060,

        [Description("LEFT")]
        Left = 12100,

        [Description("LIKE")]
        Like = 12120,

        [Description("LIMIT")]
        Limit = 12120,

        [Description("(")]
        LeftBracket = 12150,

        [Description("<")]
        LeftGuillemet = 12170,

        [Description("LOCK")]
        Lock = 12400,

        [Description("LOADING")]
        Loading = 12600,

        [Description("LONG")]
        Long = 12700,

        [Description("LOWER")]
        Lower = 12800,

        [Description("<")]
        LT = 12900,


        //M-13000
        [Description("MACRO")]
        Macro = 13300,

        [Description("MAX")]
        Max = 13350,

        [Description("MERGE")]
        Merge = 13400,

        [Description("MESSAGE_TEXT")]
        MessageText = 13500,

        [Description("MIN")]
        Min = 13550,

        [Description("-")]
        Minus = 13560,

        [Description("MINUTE")]
        Minute = 13580,

        [Description("MOD")]
        Mod = 13600,

        [Description("MODE")]
        Mode = 13700,
        
        [Description("MODIFY")]
        Modify = 13750,

        [Description("MONTH")]
        Month = 13800,

        [Description("MULTISET")]
        Multiset = 13900,


        //N-14000
        [Description("NAMED")]
        Named = 14200,

        [Description("NO")]
        No = 14300,

        [Description("NOT")]
        Not = 14400,

        [Description("NE")]
        NE = 14500,

        [Description("NULL")]
        Null = 14700,

        [Description("NEW")]
        New = 14800,

        [Description("NUMBER")]
        Number = 14900,

        [Description("NUMERIC")]
        Numeric = 14920,


        //O-15000
        [Description("ON")]
        On = 15200,

        [Description("OUT")]
        Out = 15300,

        [Description("OUTER")]
        Outer = 15500,

        [Description("OR")]
        Or = 15600,

        [Description("ORDER")]
        Order = 15700,

        [Description("OVER")]
        Over = 15800,


        //P-16000
        [Description("PARTITION")]
        Partition = 16050,

        [Description("PASSING")]
        Passing = 16070,

        [Description("+")]
        Plus = 16100,

        [Description("PERCENT")]
        Percent = 16200,

        [Description("PRESERVE")]
        Preserve = 16250,

        [Description("PROCEDURE")]
        Procedure = 16300,

        [Description("POSITION")]
        Position = 16400,

        [Description("PRIMARY")]
        Primary = 16800,        


        //Q-17000
        [Description("QUALIFY")]
        Qualify = 17500,


        //R-18000
        [Description("RANDOM")]
        Random = 18050,

        [Description("RANGE_N")]
        RangeN = 18055,

        [Description("RANK")]
        Rank = 18060,

        [Description("REAL")]
        Real = 18070,

        [Description("RECURSIVE")]
        Recursive = 18100,

        [Description("RESULT")]
        Result = 18150,

        [Description("REPLACE")]
        Replace = 18200,

        [Description("REGEXP_SUBSTR")]
        RegexpSubstr = 18300,

        [Description("REGEXP_SIMILAR")]
        RegexpSimilar = 18350,

        [Description("RIGHT")]
        Right = 18500,

        [Description(")")]
        RightBracket = 18700,

        [Description(">")]
        RightGuillemet = 18800,

        [Description("ROW")]
        Row = 18850,

        [Description("ROW_NUMBER")]
        RowNumber = 18900,


        //S-19
        [Description("SAMPLE")]
        Sample = 19100,

        [Description("/")]
        Slash = 19200,

        [Description("SECOND")]
        Second = 19250,

        [Description("SELECT")]
        Select = 19300,

        [Description("SESSION")]
        Session = 19350,

        [Description(";")]
        Semicolon = 19400,

        [Description("SET")]
        Set = 19500,

        [Description("SETS")]
        Sets = 19700,

        [Description("SMALLINT")]
        SmallInt = 19800,

        [Description("SOME")]
        Some = 19820,

        [Description("SQLCODE")]
        SqlCode = 19830,

        [Description("SQLTEXT")]
        SqlText = 19840,

        [Description("SQLEXCEPTION")]
        SqlException = 19845,

        [Description("SQLWARNING")]
        SqlWarning = 19855,

        [Description("SIGNAL")]
        Signal = 19920,

        [Description("STATISTICS")]
        Statistics = 19930,        

        [Description("SUBSTRING")]
        Substring = 19950,

        [Description("SUBSTR")]
        Substr = 19970,

        [Description("SUM")]
        Sum = 19975,


        //T-20000
        [Description("TABLE")]
        Table = 20200,

        [Description("TIME")]
        Time = 20300,

        [Description("TIMESTAMP")]
        TimeStamp = 20400,

        [Description("TITLE")]
        Title = 20450,

        [Description("THEN")]
        Then = 20500,

        [Description("THRESHOLD")]
        Threshold = 20550,

        [Description("TOP")]
        Top = 20600,

        [Description("TRANSACTION")]
        Transaction = 20620,

        [Description("TRAILING")]
        Trailing = 20630,

        [Description("TRANSLATE")]
        Translate = 20650,

        [Description("TRANSLATE_CHK")]
        TranslateChk = 20670,

        [Description("TRIGGER")]
        Trigger = 20700,

        [Description("TRIM")]
        Trim = 20800,

        [Description("TEMPORARY")]
        Temporary = 20870,

        [Description("TYPE")]
        Type = 20900,


        //U=21
        [Description("UC")]
        Uc= 21100,

        [Description("UNION")]
        Union = 21200,

        [Description("UNICODE")]
        Unicode = 21250,

        [Description("UPDATE")]
        Update = 21300,

        [Description("UPPER")]
        Upper = 21320,

        [Description("UPPERCASE")]
        UpperCase = 21330,

        [Description("USER")]
        User = 21400,

        [Description("USING")]
        Using = 21600,
        

        //V-22
        [Description("VALUE")]
        Value = 22050,

        [Description("VALUES")]
        Values = 22070,

        [Description("VARCHAR")]
        Varchar = 22200,

        [Description("VARBYTE")]
        Varbyte = 22300,

        [Description("VIEW")]
        View = 22400,

        [Description("VOLATILE")]
        Volatile = 22600,


        //W-23000
        [Description("WITH")]
        With = 23200,

        [Description("WHEN")]
        When = 23300,

        [Description("WHERE")]
        Where = 23400,

        [Description("WHILE")]
        While = 23600,

        [Description("WORK")]
        Work = 23700,


        //X-24000
        [Description("XML")]
        Xml = 24250,

        [Description("XMLEXTRACT")]
        XmlExtract = 24400,

        [Description("XMLPLAN")]
        XmlPlan = 24500,


        //Y-25000
        [Description("YEAR")]
        Year = 25500,


        //Z-26000
        [Description("ZEROIFNULL")]
        ZeroIfNull = 26200,

        [Description("ZONE")]
        Zone = 26500,
    }
}
