﻿
namespace nTeradataSqlParser
{
    public class Lexem
    {
        public LexemKind Kind;
        public string Image;
        public int LineNumber;
        public int InLinePosition;

        public override string ToString()
        {
            return string.Format("{0}: {1}", Kind, Image);
        }
    }
}
