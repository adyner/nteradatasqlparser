﻿using System.Collections.Generic;

namespace nTeradataSqlParser
{
    public class StatementParseResult
    {
        public Lexem ActionKind;
        public Identifier TargetObjectId;
        public List<Identifier> SourceObjects;
    }
}
